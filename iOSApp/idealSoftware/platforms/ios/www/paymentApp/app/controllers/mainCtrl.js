angular.module("app")
     .controller("mainCtrl", ["$scope", "$rootScope", "$location", "$route", "idealsApi", "$window", "deviceReadyService", "SharedService", "configDetailsProvider", function($scope, $rootScope, $location, $route, idealsApi, $window, deviceReadyService, SharedService, configDetailsProvider) {

        $rootScope.messageReceived = {
            data: {
                message: ""
            }
        };
                             

                             
                             
             function CreateDate() {
             var currentdate = new Date();
             var currentDateTime = currentdate.toISOString();
             return currentDateTime;
             };
                             
             document.addEventListener("deviceready", onDeviceReady, false);
                             
                             
             function onDeviceReady() {
            document.addEventListener("pause", onPause, false)//FF @05-14-19
             
             var devicePlatform = device.platform;
             var deviceID = device.uuid;
             console.log(devicePlatform+ "  " + deviceID);
             
             //Note that this callback will be fired everytime a new token is generated, including the first time.
             FCMPlugin.getToken(function(token){
                                sessionStorage.setItem("FCMToken11", token);
                                console.log(token);
                                sessionStorage.setItem("DeviceId11", device.uuid);
                                console.log(device.uuid);
                                sessionStorage.setItem("DevicePlatform11", device.platform);
                                console.log("Device Token11 -----:" + token);
                                console.log("Device ID 11-----:" + device.uuid);
                                
                                });
             
             //test Start-----
             
             FCMPlugin.onNotification(function(data){
                                      if(data.wasTapped){
                                      //Notification was received on device tray and tapped by the user.
//                                     alert( JSON.stringify(data) );
                                      $rootScope.customerMsg="";
                                      
                                      console.log( "----notification. " + JSON.stringify(data) );
                                      console.log( "----notificationObj1" + data['gcm.notification.siteId']);
                                      var msgResp = data.aps.alert.body;
                                      var dateTime = CreateDate();
//                                      var storeID = "1";
                                      
                                      var storeID = data['gcm.notification.siteId'];
//                                    deviceReadyService.sendMessageResp(msgResp,dateTime,storeID);
                                      
                                      if(sessionStorage.getItem("whenPaymentMin") == "minimisedFromPayment"){
                                      sessionStorage.removeItem("whenPaymentMin");
                                      deviceReadyService.sendMessageResp(msgResp,dateTime,storeID);
                                      }else{
                                      console.log("Already saved - msgFrmSearch");
                                      }
                                      
                                      
                                      }else{
                                      //Notification was received in foreground. Maybe the user needs to be notified.
//                                     alert( JSON.stringify(data) );
                                      $rootScope.msgAlreadySaved = true;
                                      $rootScope.customerMsg="";
                                      
                                      console.log( "----notification. " + JSON.stringify(data) );
                                      console.log( "----notificationObj1" + data['gcm.notification.siteId']);
                                      var msgResp = data.aps.alert.body;
                                      var dateTime = CreateDate();
//                                      var storeID = "1";
                                      var storeID = data['gcm.notification.siteId'];
                                      deviceReadyService.sendMessageResp(msgResp,dateTime,storeID);
                                                                            
                                      }
                                      });
                          
             //Test End----
             }
                             
              //FF @ 05-14-19
              function onPause() {
              console.log("App is minimised from Payment");
              //sessionStorage.removeItem("customerMsg");
              sessionStorage["whenPaymentMin"] = "minimisedFromPayment";
              }
                             

        $rootScope.errorCount = 0;
        $rootScope.isLogin = false;
        $rootScope.isSubscribe = false;
        $rootScope.ErrorMessage = "";
        sessionStorage.versionNo = "2.0";
        $rootScope.versionNo = sessionStorage.versionNo;
        $rootScope.mesgGreen=false;
        $rootScope.mesgRed=false;

       $rootScope.IsShopShow = configDetailsProvider.apiConnect.IsShopShow;
                             
                             
        try {
            // check api response
            $rootScope.message = "";
            $scope.redirect = function(mode) {
                sessionStorage.setItem("key", mode);
                $window.location.href = "../paymentApp/index.html";
                if ((localStorage.getItem("siteID") != null) && (localStorage.getItem("userName") != null) && (localStorage.getItem("passWord") != null)) {
                    sessionStorage.setItem("siteID", localStorage.getItem("siteID"));
                    sessionStorage.setItem("userName", localStorage.getItem("userName"));
                    sessionStorage.setItem("passWord", localStorage.getItem("passWord"));
                    sessionStorage.loggedUser = "valid";
                }
            }

            document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect.color);

            if (configDetailsProvider.apiConnect.flag == false) {
                $(".Shopdisabled").css('pointer-events', 'none');
                $(".Shopdisabled").css('opacity', '0.6');
            }


            $(".main-logo").css("background", configDetailsProvider.apiConnect.image);
            $(".main-logo").css("background-size", "contain");

            $scope.$watch(function() {
                return $rootScope.messageReceived;
            }, function(newVal, oldVal) {


                switch (newVal.data.message) {
                    case 'connect':
                        $rootScope.isSubscribe = false;
                        $rootScope.message = "";

                        if (sessionStorage.getItem("key") != "Payments") {
                            $rootScope.Loaded = true;
                        }
                        break;
                    case 'site_information':
                        $rootScope.message = "";
                         
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.apiErrorMessage = "";
                            sessionStorage.checkStoreInformation = "have value";
                            sessionStorage.setItem("siteID", newVal.data.v1.siteID);
                            // sessionStorage.storeName = store_name.name;
                          
                            sessionStorage.currentTimeStamp = newVal.data.v1.currentTimeStamp;
                            sessionStorage.storeINFORMATION = "Store: " + newVal.data.v1.address + " " + newVal.data.v1.city + ", " + " " + newVal.data.v1.state + " " + newVal.data.v1.zip + " " + "Phone: " + newVal.data.v1.phone;
                            $rootScope.storeINFO = sessionStorage.storeINFORMATION;
                          
                            console.log("storeINFO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + $rootScope.storeINFO);


                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.ShowLoader = false;
                        }
                        break;

                    case 'authenticate':
                        $rootScope.isLogin = true;
                        $rootScope.message = "";
                        $rootScope.apiErrorMessage = "";
                        if (newVal.data.v1.errorDescription === "Invalid Username and/or Password") {
                            $rootScope.errorCount++;
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.mesgRed=true;
                            if ($rootScope.errorCount >= 3) {
                           // $rootScope.msgClass="";
                            // $rootScope.msgClass="error";
                             $rootScope.message = "Invalid username and/or password. If you have forgotten your username and/or password try retrieving it by following the 'Forgot Password' link. If you have not signed up to use this site, you may do so by clicking the Sign Up button.";
                            }
                        } else if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.errorCount = 0;
                            sessionStorage.loggedUser = "valid";
                            sessionStorage["authenticate"] = "";
                            sessionStorage["pay_stored_card"] = "";
                            $rootScope.ShowLoader = false;
                            $rootScope.mesgGreen=true;
                            $rootScope.message = "Credentials Verified";
                            var Message = newVal.data.message;
                            if (Message == "authenticate"){
                                sessionStorage.customerID = newVal.data.v1.customerID;
                                sessionStorage.validToken = newVal.data.v1.token;
                            }

                            idealsApi.customer_informationReq();

                            if ($rootScope.rememberMe == true) {
                                var siteID = sessionStorage.getItem("siteID");
                                localStorage.setItem("siteID", siteID);
                                localStorage.setItem("userName", $rootScope.login.username);
                                localStorage.setItem("passWord", $rootScope.login.password);
                            } else {

                                sessionStorage.setItem("userName", $rootScope.login.username);
                                sessionStorage.setItem("passWord", $rootScope.login.password);
                                sessionStorage.loggedUser = "valid";
                                localStorage.removeItem("siteID");
                                localStorage.removeItem("userName");
                                localStorage.removeItem("passWord");
                            }

                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.errorCount = 0;
                            $rootScope.ShowLoader = false;
                        }
                        break;

                    case 'list_accounts':
                        $rootScope.ShowLoader = false;
                        $rootScope.Loaded = true;

                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage.removeItem("selectedPayment");
                            sessionStorage.userFLName = newVal.data.v1.customerInfo.firstName + " " + newVal.data.v1.customerInfo.lastName;
                            sessionStorage.userName = newVal.data.v1.customerInfo.userName;
                            sessionStorage["list_accounts"] = JSON.stringify($rootScope.messageReceived);

                            if (sessionStorage.getItem("key") === "Payments") {
                                $rootScope.Loaded = true;
                                $route.reload();
                                sessionStorage.setItem("key", "");
                            } else {
                                $location.path("/account");
                            }
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;
                    case 'logout':
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.message = "";
                            sessionStorage.loggedUser = "null";
                            $location.path("/login");
                        } else {
                            $rootScope.message = "";
                            $rootScope.ShowLoader = false;
                            $route.reload();
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;
                    case 'update_account':
                        $rootScope.message = "";
                        newVal.data.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.accountSubmitMessage = "Your Account has been updated.";
                            $location.path("/changeAcSubmit");
                        } else {
                            $rootScope.message = "";
                            $rootScope.ShowLoader = false;
                            $route.reload();
                            $rootScope.message = newVal.data.v1.errorDescription;
                        }
                        break;
                    case 'reset_password':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.accountSubmitMessage = "Your Account has been updated.";
                            $location.path("/changeAcSubmit");
                        } else {
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $rootScope.ShowLoader = false;
                            $route.reload();
                        }
                        break;
                    case 'signup':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            $rootScope.errorCount = 0;
                            $rootScope.accountSubmitMessage = "Your Account has been created. Please make a note of your username and password.";
                            $location.path("/changeAcSubmit");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $route.reload();
                        }
                        break;
                    case 'list_stored_cards':
                        $rootScope.message = "";
                        $rootScope.ShowLoader = false;
                        if (newVal.data.v1.errorDescription === "Successful") {
                            if (newVal.data.v1.cards != null) {
                                sessionStorage["list_cards"] = JSON.stringify($rootScope.messageReceived);
                            } else {
                                sessionStorage["list_cards"] = null;
                            }
                            $rootScope.errorCount = 0;
                            $location.path("/checkInfo");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $route.reload();
                        }
                        break;
                    case 'pay_stored_card':
                        $rootScope.message = "";
                        sessionStorage.removeItem("selectedPayment");
                        $rootScope.ShowLoader = false;


                        //..........android and ios back button diable .............
                        document.addEventListener("deviceready", onDeviceReady, false);

                        function onDeviceReady() {
                            document.addEventListener("backbutton", function(e) {
                                e.preventDefault();
                            }, false);
                        }

                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["pay_stored_card"] = JSON.stringify($rootScope.messageReceived);
                            $rootScope.errorCount = 0;
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $location.path("/paymentDecline");
                        }
                        break;
                    case 'customer_information':
                        if (sessionStorage["pay_stored_card"] === "") {
                            sessionStorage.customerInformation = JSON.stringify(newVal.data.v1.customerInfo);

                            $rootScope.firstName = JSON.parse(sessionStorage.customerInformation).firstName;
                            $rootScope.lastName = JSON.parse(sessionStorage.customerInformation).lastName;
                        } else {
                            $rootScope.message = "";
                            $rootScope.ShowLoader = false;

                            //............android and ios back button diable ..............

                            document.addEventListener("deviceready", onDeviceReady, false);

                            function onDeviceReady() {
                                document.addEventListener("backbutton", function(e) {
                                    e.preventDefault();
                                }, false);
                            }

                            if (newVal.data.v1.errorDescription === "Successful") {
                                sessionStorage["cust_info"] = JSON.stringify($rootScope.messageReceived);
                                $rootScope.errorCount = 0;
                                $location.path("/makePayment");
                            } else {
                                $rootScope.ShowLoader = false;
                                $rootScope.message = newVal.data.v1.errorDescription;
                                $route.reload();
                            }
                        }
                        break;

                    case 'payment_history':

                        $rootScope.message = "";
                        if (newVal.data.v1.errorDescription === "Successful") {
                            sessionStorage["pay_hist"] = JSON.stringify($rootScope.messageReceived);
                            $rootScope.errorCount = 0;
                            $location.path("/PaymentHistory");
                        } else {
                            $rootScope.ShowLoader = false;
                            $rootScope.message = newVal.data.v1.errorDescription;
                            $route.reload();
                        }
                        break;

                }

            });
        } catch (e) {
            console.log(e);
        }

    }]);
