﻿function setchatheight() {
    var win_height = $(window).height();
    $(".block-area").css('height', (win_height - 60) + "px");
}

$(document).ready(function () {
    setchatheight();
});


$(window).resize(function () {
    setchatheight();
});

$(window).load(function () {
    setchatheight();
});
