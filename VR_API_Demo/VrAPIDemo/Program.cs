﻿using Demo.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VrStorePubSub;

namespace VrAPIDemo
{
	class Program
	{
		static void Main(string[] args)
		{
			//Send a site_information request and listen for a response

			VrStorePubSub.VrStorePubSubClass VrStore = null;
			var Finished = false;
			try
			{
				VrStore = new VrStorePubSubClass();
				Finished = false;
			}
			catch (Exception E)
			{
				Log.Error(E, "ERROR creating VrStore class.");
			}
			Log.LastTimeStarted = DateTime.Now;
			while (!Finished)
			{
				try
				{

					Console.WriteLine("");
					Console.Write(">");
					var Cmd = Console.ReadLine();
					Finished = Cmd.ToUpper() == "EXIT";
					if (!Finished)
					{
						if (Cmd.ToUpper().Contains("SEND"))
						{
							VrStore.SendSiteInformationRequest();
						}
						else if (Cmd.ToUpper().Contains("RESET"))
						{
							VrStore.ResetPubSubConnection();
						}

					}
				}
				catch(Exception E)
				{
					Log.Error(E, "Error in commandline. ");
				}

			}
			try
			{
				Log.Info("=================");
				Log.Info("Shutting down...");
				VrStore.Disconnect();
				Log.Info("   ...DONE. Goodbye.");
				Log.Info("=================");
			}
			catch (Exception E)
			{
				Log.Error(E, "Error shutting down VR Store Demo. ");
			}

		}
	}
}
