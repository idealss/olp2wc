﻿using System;
using System.Collections.Concurrent;
using System.Text;
using System.Web;


namespace Demo.Logging
{

    public static class Log
	{
		enum LogType {Info, Debug, Warn, Error };
        public static Object LogLock = new object();
		class LogNode
		{
			public string Content
			{
				get;
				set;
			}
			public LogType Severity
			{
				get;
				set;
			}
			public LogNode()
			{
				Severity = LogType.Info;
				Content = "";
			}
        }

        public static DateTime LastTimeLongPollingMessageRecieved = DateTime.MinValue;
        public static DateTime LastTimeLongPollingMessageSent = DateTime.MinValue;
        public static DateTime LastTimeRegularMessageSent = DateTime.MinValue;
        public static DateTime LastTimeRegularMessageReceived = DateTime.MinValue;
        public static DateTime LastTimeMetaMessageReceived = DateTime.MinValue;
        public static DateTime LastTimeMetaMessageSent = DateTime.MinValue;
        public static string LastErrorMessage = "";
        public static DateTime LastTimeErrorReceived = DateTime.MinValue;
        public static DateTime LastTimeStarted = DateTime.MinValue;
        public static DateTime LastTimeAutoRecovered = DateTime.MinValue;
        public static string LastAutoRecoverCause = "";
        public static int MaxSize = 10000;
		public static bool CacheUntilError = false;
		public static bool IgnoreLongPolling = true;
		public static bool IgnoreMETA = false;
        public static string ProcessName = "DemoVrAPI";
        static ConcurrentQueue<LogNode> cq = null;

        static void ProcessMessage(string AMessage, LogType ASeverity)
		{
            lock (LogLock)
            {
                string LMessage = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + "  " + AMessage;
                if (ASeverity == LogType.Error)
                {
                    LastErrorMessage = LMessage;
                    LastTimeErrorReceived = DateTime.Now;
                }
                if (!AMessage.ToLower().Contains("error"))
                {
                    if (IgnoreLongPolling)
                    {
                        if (AMessage.Contains("long-polling"))
                        {
                            return;
                        }
                    }
                    if (IgnoreMETA)
                    {
                        if (AMessage.Contains("META"))
                        {
                            return;
                        }
                    }
                }
                switch (ASeverity)
                {
                    case LogType.Debug:
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine(LMessage);
                            Console.ResetColor();
                            break;
                        }
                    case LogType.Info:
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine(LMessage);
                            Console.ResetColor();
                            break;
                        }
                    case LogType.Warn:
                        {
                            Console.ForegroundColor = ConsoleColor.DarkMagenta;
                            Console.WriteLine(LMessage);
                            Console.ResetColor();
                            break;
                        }
                    case LogType.Error:
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(LMessage);
                            Console.ResetColor();
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
		}

		public static void Debug(string AMessage)
		{
			ProcessMessage(AMessage, LogType.Debug);
		}

		public static void Warn(string AMessage)
		{
			ProcessMessage(AMessage, LogType.Warn);
		}

		public static void Info(string AMessage)
		{
			ProcessMessage(AMessage, LogType.Info);
		}

		/*
		public static void Error(string AMessage)
		{
			Exception E = new Exception(AMessage);
			Error(E);
		}
		*/

		public static void Error(string Message, Exception E)
		{
			Error(E, Message);
		}

		public static void Error(Exception E, string Message = "")
		{
            lock (LogLock)
            {
                string LNow = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt");
                StringBuilder sb = new StringBuilder();
                if (E != null)
                {
                    sb.AppendLine(LNow + "  Exception.    " + Message);
                    sb.AppendLine("   ------------------------------");
                    sb.AppendLine("     ---Exception: " + E.Message);
                    sb.AppendLine("     --Stack Trace:--");
                    sb.AppendLine("     " + E.StackTrace);
                    sb.AppendLine("     --End Stack trace--");
                    sb.AppendLine("     --Source: " + E.Source);
                    if (E.TargetSite != null) sb.AppendLine("     --Target: " + E.TargetSite.ToString());
                    Exception excpt = E.InnerException;
                    while (excpt != null)
                    {
                        sb.AppendLine("    --Inner Exception: " + excpt.Message);
                        sb.AppendLine("      --Stack Trace:--");
                        sb.AppendLine("      " + excpt.StackTrace);
                        sb.AppendLine("      --End Stack trace--");
                        sb.AppendLine("      --Source: " + excpt.Source);
                        if (excpt.TargetSite != null) sb.AppendLine("      --Target: " + excpt.TargetSite.ToString());

                        excpt = excpt.InnerException;
                    }
                    sb.AppendLine("   ------------------------------");
                }
                else
                {
                    sb.AppendLine(LNow + "  Error.    " + Message);
                }
                ProcessMessage(sb.ToString(), LogType.Error);
            }
		}
	}
}
 