﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cometd.Client;
using Cometd.Bayeux.Client;
using Cometd.Client.Transport;
using Cometd.Bayeux;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CometD.Client.Ideal.VrStore.API;
using Demo.Logging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Ideal.LongPollingTransport;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Net;
using System.Runtime.Remoting.Messaging;

namespace VrStorePubSub
{
	/*  This namespace provides the publish/subscribe logic for a VrStore implementation*/
	//TODO: these are hardwired for the demo. For your own production use you ought to read the values out of an encrypted configuration file or a database.


	public class VrStoreChannelListener : IMessageListener
	{
		protected Action<IClientSessionChannel, Cometd.Bayeux.IMessage> fCallback;
		public VrStoreChannelListener(Action<IClientSessionChannel, Cometd.Bayeux.IMessage> callback)
		{
			fCallback = callback;
			Log.Debug("Listener constructed.");
		}

		void IMessageListener.onMessage(IClientSessionChannel channel, Cometd.Bayeux.IMessage message)
		{
			try
			{
				if (fCallback == null)
				{
					throw new Exception("VrStoreListener callback not assigned.");
				}
				StringBuilder sb = new StringBuilder();
				sb.AppendLine("<<<<<<<<<<<<<");
				sb.AppendLine("Listener received incoming message. Channel: " + channel.Id + "  Message: " + message.JSON);
				sb.AppendLine("<<<<<<<<<<<<<");
				Log.Debug(sb.ToString());
				Log.LastTimeRegularMessageReceived = DateTime.Now;
				fCallback(channel, message);
			}
			catch (Exception E)
			{
				Log.Error(E, "Error VrStoreChannelListener.onMessage");
			}

		}
	}
	public class VrStorePubSubClass
	{

		private static readonly string DEMO_CORPID = "1";
		private static readonly string DEMO_OLAFID = "241";
		private static readonly string DEMO_SITEID = "1";
		private static readonly string DEMO_TOKEN = "04b2db9589e96909e28972e2e80c77daa69d74f627b6d169e4785ff7d452cd94";//"41cf45bfdd6f5090c4910aa741120a4489eac7caa8490687aee17390581300b1";
		private static readonly string DEMO_PRIVATEKEY = "2c24faefc0331e9aa93ff2454f265aa69b3ed1c1e2a574ae39e4221689d1468c";//"af4ada65f1694623378290832c9326a0c5e7ffe6afdb70a78ee5adea4eac828b";

		//TODO: for production use this URL instead: https://pubsub.idealss.net/
		private static readonly string DEMO_PUBSUBURL = "https://pubsub-test.idealss.net/";
		private static readonly string DEMO_CORPDESCRIPTION = "VR API DEMO Corporation";
		private static readonly string DEMO_SITENAME = "VR API Demo Site";

		Task Task_SubscribeToChannels = null;
		private DateTime FLastReset;
		private readonly double RESET_DELAY = 10.0;

		private ConcurrentDictionary<string, BayeuxClient> SubscribedChannels;
		protected ConcurrentDictionary<int, SiteClient> SiteList;
		public bool SingleSiteMode = false;

		//site_information object for sending JSON
		public class SiteInformationRequest
		{
			public class _v1
			{
				public string messageID = Guid.NewGuid().ToString().ToLowerInvariant();
				public int siteID = Convert.ToInt32(DEMO_SITEID);
				public string returnChannel = "";
				public string currentTimeStamp = "";
				public _v1(int ASiteID, string ARespondChannel)
				{
					returnChannel = ARespondChannel;
					siteID = ASiteID;
					currentTimeStamp = Convert.ToString(DateTime.Now);
				}
			}
			public _v1 v1;
			public string message = VrStoreHelper.VrStore_site_information;
			public SiteInformationRequest(int ASiteID, string subChannel)
			{
				v1 = new _v1(ASiteID, subChannel);
			}
			public override string ToString()
			{
				JsonSerializerSettings serializer = new JsonSerializerSettings();
				serializer.DateFormatHandling = 0; //ISO format
				serializer.NullValueHandling = NullValueHandling.Ignore;
				return JsonConvert.SerializeObject(this, serializer);
			}
		}

		public void SendSiteInformationRequest()
		{
			//this is our main client code for sending a site_information request to the store (server)
			SiteClient LSite = GenerateNewSiteClient();
			if (LSite.Client == null)
			{
				CreateClient(LSite);
			}


			string LRespondChannel = GenerateRespondChannel(LSite);
			string LPublishChannel = LSite.Channel;
			SiteInformationRequest si = new SiteInformationRequest(Convert.ToInt32(DEMO_SITEID), LRespondChannel);
			string s = si.ToString();
			Subscribe(LSite, DoOnSiteInformationResponse, LRespondChannel);
			PublishSync(LSite, LPublishChannel, s);
		}

		public void DoOnSiteInformationResponse(IClientSessionChannel channel, Cometd.Bayeux.IMessage message)
		{
			//process response from having sent a fully populated Customer_info to an nSite site
			//this should simply be an ack
			string sa = "";
			// Handles the message
			try
			{
				sa = message.ToString();
				JObject data = JObject.Parse(sa);
				JObject json = (JObject)data["data"];
				string Message = (string)json["message"];
				int fromSiteID = 0;

				Log.Info("messageID: " + Message);

				if (Message == VrStoreHelper.VrStore_site_information) //this is a response to our site_information request we sent to the store  (server)
				{
					if (json["data"] != null)
					{
						if (json["data"]["v1"] != null)
						{
							//for this demo we wil just log out the values...you will want to save these values to persistent storage or process them 
							if (json["data"]["v1"]["ipAddress"] != null) Log.Info("IP Address: "+(string)json["data"]["v1"]["ipAddress"]);
							if (json["data"]["v1"]["publicIP"] != null) Log.Info("Public IP: " + (string)json["data"]["v1"]["publicIP"]);
							if (json["data"]["v1"]["currentTimeStamp"] != null) Log.Info("Current Timestamp: " + (string)json["data"]["v1"]["currentTimeStamp"]);
							if (json["data"]["v1"]["siteID"] != null) Log.Info("SiteID: " + (int)json["data"]["v1"]["siteID"]);
							if (json["data"]["v1"]["storeName"] != null) Log.Info("Store Name: " + (string)json["data"]["v1"]["storeName"]);
							if (json["data"]["v1"]["address"] != null) Log.Info("Address: " + (string)json["data"]["v1"]["address"]);
							if (json["data"]["v1"]["city"] != null) Log.Info("City: " + (string)json["data"]["v1"]["city"]);
							if (json["data"]["v1"]["state"] != null) Log.Info("State: " + (string)json["data"]["v1"]["state"]);
							if (json["data"]["v1"]["zip"] != null) Log.Info("Zip Code: " + (string)json["data"]["v1"]["zip"]);
							if (json["data"]["v1"]["phone"] != null) Log.Info("Phone: " + (string)json["data"]["v1"]["phone"]);
							if (json["data"]["v1"]["storeNumber"] != null) Log.Info("Store Number: " + (string)json["data"]["v1"]["storeNumber"]);
							if (json["data"]["v1"]["forceMinDue"] != null) Log.Info("Force Minimum Due: " + (bool)json["data"]["v1"]["forceMinDue"]);
							if (json["data"]["v1"]["customerServiceEmail"] != null) Log.Info("Customer Service Email: " + (string)json["data"]["v1"]["customerServiceEmail"]);
							if (json["data"]["v1"]["locationID"] != null) Log.Info("Location ID: " + (string)json["data"]["v1"]["locationID"]);
							if (json["data"]["v1"]["successful"] != null) Log.Info("Successful: " + (bool)json["data"]["v1"]["successful"]);
							if (json["data"]["v1"]["errorDescription"] != null) Log.Info("Error Description: " + (string)json["data"]["v1"]["errorDescription"]);
							if (json["data"]["v1"]["processTime"] != null) Log.Info("Process Time: " + (string)json["data"]["v1"]["processTime"]);
							if (json["data"]["v1"]["agentVersion"] != null) Log.Info("Agent Version: " + (string)json["data"]["v1"]["agentVersion"]);

						}
					}

					//SiteInformationResponseWasReceived = true;
					if (channel != null)
					{
						channel.unsubscribe(); //channel was temporary return channel
					}
				}
				else
				{
					//ignore everything else
				}


			}
			catch (Exception E)
			{
				Log.Error(E, "   ERROR On DoOnSiteInformationResponse.  " + E.Message + " Message: " + sa);
			}
		}

		protected Boolean CheckForConnectionError(Exception E)
		{
			if (E.Message.ToLower().Contains("connect")
			   || E.Message.ToLower().Contains("server not found")
			   || E.Message.ToLower().Contains("unable to complete network request")
				|| E.Message.Contains("(503) Server Unavailable.")
				|| E.Message.Contains("(502) Bad Gateway.")
				|| E.Message.Contains("(504)")
				|| E.Message.Contains("(505)")
				|| E.Message.Contains("(500)")
				|| E.Message.Contains("(501)")
				|| E.Message.Contains("Unable to connect")
				|| E.Message.ToLower().Contains("connection was closed")
				|| E.Message.ToLower().Contains("unknown client")
				|| E.Message.ToLower().Contains("request was aborted")
			   )
			{
				ResetPubSubConnection();
				Log.LastAutoRecoverCause = E.Message;
				return false;
			}
			else return true;
		}

		protected string GenerateRespondChannel(SiteClient ASite)
		{
			if (ASite == null)
			{
				return "";
			}

			string LGuidStr = Guid.NewGuid().ToString().ToLowerInvariant();
			//strip non-ascii
			LGuidStr = Regex.Replace(LGuidStr, @"[^\u0000-\u007F]+", string.Empty);

			return "/vr_store/" + ASite.GroupID + "/" + ASite.Client.Id + "/Respond" + LGuidStr;
		}

		protected string GenerateReturnChannel(SiteClient ASite)
		{
			return "/vr_store/" + ASite.GroupID + "/" + ASite.Client.Id + "/return";
		}

		public void UnsubcribeAllChannels()
		{
			try
			{
				foreach (KeyValuePair<int, SiteClient> ch in SiteList)
				{
					UnsubscribePublicChannel(ch.Value, "", false);
				}
				SubscribedChannels.Clear();

			}
			catch (Exception E)
			{
				Log.Error(E, "   Error unsubscribing to all channels. ");
				//we WANT to merely log and then throwaway this exception, because this function is part of processes that need subsequent steps to execute
			}
		}
		public void UnsubscribePublicChannel(SiteClient Site, string Channel = "", Boolean RemoveFromChannelList = true)
		{
			string ch = Channel;
			try
			{
				if (Site == null)
				{
					Log.Warn("   Cannot unsubscribe to channel because Site is NULL. Channel: " + Channel);
				}
				else
				{
					if (ch == "")
					{
						ch = Site.Channel;
					}
					Log.Info("   Unsubscribe channel: " + ch);

					BayeuxClient LClient = null;
					string LKey = GetChannelKey(Site, ch);
					if (SubscribedChannels.TryGetValue(LKey, out LClient))
					{
						if (LClient == null)
						{
							Log.Warn("       Client not found for key: " + LKey);
						}
						else
						{
							Log.Info("   Found client: " + LClient.Id);
							IClientSessionChannel LChannel = LClient.getChannel(ch);
							if (LChannel == null)
							{
								Log.Warn("        Channel not found for client ID: " + LClient.Id);
							}
							else
							{
								Log.Info("     Unsubscribing...");
								LChannel.unsubscribe(Site.Listener);
								Log.Info("     Unsubscribed.");
							}

							if (RemoveFromChannelList)
							{
								Log.Info("     Removing from Channel list...");
								SubscribedChannels.TryRemove(LKey, out LClient);
								Log.Info("     Removed from Channel list.");
							}
						}
					}
					else
					{
						Log.Warn("      Channel not found in list of subscribed channels. ");
					}
				}
			}
			catch (Exception E)
			{
				Log.Error(E, "   Error unsubscribing to channel:" + ch);
				//we WANT to merely log and then throwaway this exception, because this function is part of processes that need subseqauent steps to execute
			}

		}
		public int Count
		{
			get
			{
				return SubscribedChannels.Count;
			}
		}
		public bool IsSingleSite { get; private set; }
		protected string GetChannelKey(SiteClient site, string Channel)
		{
			return site.PubSubUrl + Channel;
		}
		public void SubscribePublicChannel(SiteClient site, Action<IClientSessionChannel, Cometd.Bayeux.IMessage> callback, string Channel)
		{
			try
			{
				BayeuxClient LClient = null;
				string LKey = GetChannelKey(site, Channel);

				if (!SubscribedChannels.TryGetValue(LKey, out LClient))
				{

					LClient = Subscribe(site, callback, Channel, true);
					SubscribedChannels.TryAdd(LKey, LClient);
				}
				else
				{
					if (site.Client == null)
					{
						Log.Info("  ClientID: " + LClient.Id + ".  Adding Client to this site: " + site.SiteID);
						site.Client = LClient; //populated by if clause above 
					}
					//this client's channel was subscribed when the channel was created

					//IClientSessionChannel publishChannel = LClient.getChannel(Channel);
					//IMessageListener Listener = new VrStoreChannelListener(callback);
					//Log.Info("   Subscribe to channel: " + publishChannel.Id + " with ClientID: " + LClient.Id );
					////publishChannel.Session.addExtension(new IdealAPIExtension(new VrStoreSetup(site)));
					//publishChannel.subscribe(Listener);

				}
			}
			catch (Exception E)
			{
				Log.Error(E, "   Error subscribing to public channel:" + site.Channel);
				CheckForConnectionError(E);
			}

		}

		private BayeuxClient Subscribe(SiteClient site, Action<IClientSessionChannel, Cometd.Bayeux.IMessage> callback, string Channel, bool ReuseExistingBayeuxClient = true)
		{
			try
			{
				BayeuxClient LClient = CreateClient(site, ReuseExistingBayeuxClient);
				IClientSessionChannel publishChannel = LClient.getChannel(Channel);
				IMessageListener Listener = new VrStoreChannelListener(callback);
				Log.Info("   Subscribe to channel: " + publishChannel.Id + " with ClientID: " + LClient.Id + "  Reuse Existing Client: " + ReuseExistingBayeuxClient);
				//publishChannel.Session.addExtension(new IdealAPIExtension(new VrStoreSetup(site)));
				publishChannel.subscribe(Listener);
				site.Listener = Listener;
				return LClient;
			}
			catch (Exception E)
			{
				Log.Error(E, "   Error subscribing to channel:" + site.Channel);
				CheckForConnectionError(E);
				return null;
			}
		}

		public void Publish(IClientSessionChannel publishChannel, string Msg)
		{
			try
			{
				Log.Info("   Publish. Publishing to Channel [" + publishChannel.Id + "]. Message: " + Msg);
				Dictionary<string, object> dict = VrStoreHelper.deserializeToDictionary(Msg);
				publishChannel.publish(dict);
			}
			catch (Exception E)
			{
				Log.Error(E, "   Publish. ERROR Publishing to Channel [" + publishChannel.Id + "]: " + Msg);
				CheckForConnectionError(E);
				throw;
			}
		}
		protected SiteClient GetSiteBySiteID(int SiteID)
		{
			//lock (SiteList) ;
			SiteClient site = null;
			if (SiteList.TryGetValue(SiteID, out site))
			{
				return site;
			}
			else
			{
				Log.Warn("[GetSiteBySiteID] Can not find site " + SiteID);
			}

			return null;
		}

		protected bool VerifyCorpSite(int ASiteID)
		{
			try
			{
				//look through CorpSites to verify
				if (SiteList.ContainsKey(ASiteID))
				{
					return (SiteList[ASiteID] != null);
				}
			}
			catch (Exception E)
			{
				Log.Error(E, "     ERROR. VerifyCorpSite. SiteID: " + ASiteID);
			}
			return false;
		}

		public void OnPublicChannelMessageReceived(IClientSessionChannel channel, Cometd.Bayeux.IMessage message) //not needed for demo
		{
			string sa = "";
			// Handles the message
			try
			{
				sa = message.ToString();
				JObject data = JObject.Parse(sa);
				JObject json = (JObject)data["data"];
				string Message = (string)json["message"];
				int fromSiteID = 0;
				if ((json["data"] != null) & (json["data"]["v1"] != null) & (json["data"]["v1"]["SiteID"] != null))
				{
					fromSiteID = (int)json["data"]["v1"]["SiteID"];
				}

				//verify that this site is one of this instance's corporation sites...
				if (VerifyCorpSite(fromSiteID))
				{
					Log.Info("OnPublicChannelMessageReceived. Incoming channel: " + channel.Id + " Message element: " + Message + "  fromSiteID: " + fromSiteID);
					if (Message == VrStoreHelper.VrStore_site_information)
					{
						//this is a public channel request for site_information, from an vr_store site
						//run in worker thread....
						Task tsk = new Task(() => ProcessSiteInformationRequest(json, fromSiteID));
						tsk.Start();

					}
					else
					{
						Log.Info("   [OnPublicChannelMessageReceived] Message received, but not processed: " + Message);
					}
				}
				else
				{
					//not for this corp, so ignore  (if exceptions, add their processing here....)
					//Log.Info("[VrStore] OnPublicChannelMessageReceived, but not for this corp. Incoming channel: " + channel.Id + " Message: " + sa);
				}
			}
			catch (Exception E)
			{
				Log.Error(E, "   [VrStore] ERROR On MessageReceived.  " + E.Message + " Message: " + sa);
				CheckForConnectionError(E);
			}

		}

		private void ProcessSiteInformationRequest(JObject json, int fromSiteID)
		{
			//for a server (store) you woul need to flesh this out. This demo is just a client, so we will leave this unimplemented
			throw new NotImplementedException();
		}

		private void PublishSync(SiteClient ASite, string APublishChannel, string AContent)
		{
			string LURL = ""; //ASite.PubSubUrl + APublishChannel;
			string LClientId = ""; // ASite.Client.Id;
								   //do HTTP POST to LURL
			if (ASite != null)
			{
				LURL = ASite.PubSubUrl + APublishChannel;
			}
			else
			{
				throw new Exception("[PublishSync] No Site.");
			}
			if (ASite.Client != null)
			{
				LClientId = ASite.Client.Id;
			}
			else
			{
				throw new Exception("[PublishSync] No Client assigned for Site.");
			}

			JObject message = new JObject();
			//do synchronous equivalent of publish
			string LContent = "";
			//add extension fields and encode data into base64, etc. into LContent
			try
			{
				//Log.Info("[PublishSync]-------------------");
				JObject env = new JObject();
				//json = JsonConvert.SerializeObject(message, Formatting.Indented);
				Log.Info("[PublishSync] (Channel: "+APublishChannel+") JSON to wrap: " + AContent);
				JObject msg = new JObject();
				msg["channel"] = APublishChannel;
				JObject LMessageObj;
				LMessageObj = JObject.Parse(AContent);
				msg["data"] = LMessageObj;// json;
				msg["clientId"] = LClientId;
				msg["id"] = GetNewPublishSyncId();
				string json = msg.ToString();

				var Salt = VrStoreHelper.getSalt();

				env.Add("api", VrStoreHelper.VrStore_api);

				if (LMessageObj["message"] != null) env.Add("message", (string)LMessageObj["message"]);
				env.Add("token", ASite.token);
				env.Add("salt", Salt);
				env.Add("signature", VrStoreHelper.MakeSignature(Salt, json, ASite.PrivateKey));
				env.Add("data", VrStoreHelper.Base64Encode(json));
				message[Cometd.Bayeux.Message_Fields.EXT_FIELD] = env;//JsonConvert.SerializeObject(env);//env.ToString(); //VrStoreHelper.deserializeToDictionary(env.ToString());
																	  //message["Data"] = new object();
				message["channel"] = msg["channel"];
				message["data"] = "{}";// json;
				message["clientId"] = msg["clientId"];
				message["id"] = msg["id"];
				message["originating-IP"] = CurrentHostName();

				LContent = message.ToString();
				//Log.Info("   Going to POST this exact content: " + LContent);

				//if (env.ToString() != "{}") Log.Info("   IdealAPIExtension Bayeaux Message Field [EXT_FIELD]: " + env.ToString());

				//Post WebRequest
				System.Net.ServicePointManager.Expect100Continue = false;
				string Result = PostSync(LURL, LContent);

				/*
				HttpWebRequest req = (HttpWebRequest)WebRequest.Create(LURL);
				req.ContentLength = LContent.Length;
				req.Method = "POST";
				req.ContentType = "application/json;charset=UTF-8";
				req.Proxy = null;
				Log.Info("   writing content to " + LURL);
				Stream strm = req.GetRequestStream();
				Log.Info("   got request stream");
				byte[] byteArray = Encoding.UTF8.GetBytes(LContent);
				//Console.WriteLine("Sending message(s): {0}", exchange.content);

				// Write to the request stream.
				strm.Write(byteArray, 0, LContent.Length);
				Log.Info("   wrote to byteArray");

				strm.Close();
				Log.Info("   Closed request stream. ");

				Log.Info("   POSTING. Now getting response...");
				WebResponse resp = req.GetResponse();
				Log.Info("   Synchronous response received...");
				HttpStatusCode LStatus = ((HttpWebResponse)resp).StatusCode;
				if (LStatus == HttpStatusCode.OK)
				{
					//success
					Log.Info("      ...SUCCESS.");
				}
				else
				{
					string LDescription = ((HttpWebResponse)resp).StatusDescription;
					Log.Error("   ---Error---. " + LDescription);
				}
				resp.Close();
				*/

				//Log.Info("[PublishSync]  DONE.------------");
			}
			catch (Exception E)
			{
				Log.Error(E, "[ PublishSync(" + ASite.ToString() + ", " + APublishChannel + ", " + AContent + ") ] ");
				CheckForConnectionError(E);
			}

		}
		private string CurrentHostName()
		{
			return (Dns.GetHostName());

		}
		private string PostAsync(string AURL, string AData)
		{
			try
			{
				var httpClient = new HttpClient();
				Log.Info("[PostAsync(" + AURL + ", " + AData + ")]");
				StringContent sc = new StringContent(AData, System.Text.Encoding.UTF8, "application/json");

				Task<HttpResponseMessage> response = httpClient.PostAsync(AURL, sc);
				HttpResponseMessage resp = response.Result;
				Log.Info("   [PostAsync]  Got Result.");
				resp.EnsureSuccessStatusCode();

				Task<string> content = resp.Content.ReadAsStringAsync();
				string cont = content.Result;
				Log.Info("[PostAsync]  Ended Async read of Result. Result: " + cont);

				return cont;
			}
			catch (Exception E)
			{
				Log.Error(E, "[PostAsync(" + AURL + ", " + AData + ")]");
				CheckForConnectionError(E);
				return ("");
			}
		}
		private string PostSync(string AURL, string AData)
		{
			try
			{
				var httpClient = new HttpClient();
				//Log.Info("[PostSync(" + AURL + ", " + AData + ")]");
				StringContent sc = new StringContent(AData, System.Text.Encoding.UTF8, "application/json");

				Task<HttpResponseMessage> response = httpClient.PostAsync(AURL, sc);
				if (response != null)
				{
					response.Wait();
					HttpResponseMessage resp = response.Result;
					//Log.Info("   [PostSync]  Got Result.");
					resp.EnsureSuccessStatusCode();
					//Log.Info("   [PostSync]  About to read string Async...");
					Task<string> content = resp.Content.ReadAsStringAsync();
					content.Wait();
					string cont = content.Result;
					Log.Info("[PostSync]  Ended Sync read of Result. Result: " + cont);

					return cont;
				}
				else
				{
					throw new Exception("[PostSync] Response to HttpClient PostSync is null");
				}
			}
			catch (Exception E)
			{
				Log.Error(E, "[PostSync(" + AURL + ", " + AData + ")]");
				CheckForConnectionError(E);
				return ("");
			}
		}
		private string GetNewPublishSyncId()
		{
			return "Sync" + g_PubSyncId++;
		}

		private void SubscribeToChannels()

		{

			try
			{
				Log.Info("  Subscribing to public channels...");
				foreach (KeyValuePair<int, SiteClient> p in SiteList)
				{
					SubscribePublicChannel(p.Value, OnPublicChannelMessageReceived, p.Value.Channel);
					break;
				}
				Log.Info("  Subscribed to public channels.");
			}
			catch (Exception E)
			{
				Log.Error(E, "ERROR SubscribeToChannels.");
				CheckForConnectionError(E);
			}
		}
		public void Disconnect()
		{
			try
			{
				UnsubcribeAllChannels();
				foreach (KeyValuePair<string, BayeuxClient> p in SubscribedChannels)
				{
					try
					{
						Log.Info("   Disconnecting from " + p.Key);
						if (!p.Value.waitForEmptySendQueue(10000))
						{
							Log.Info("      Timed out waiting for send queue to be emptied...aborting....");
							p.Value.abort();
						}
						p.Value.disconnect();
					}
					catch (Exception E)
					{
						Log.Error(E, "      Error disconnecting from " + p.Value.Id);
					}
				}
				SubscribedChannels.Clear();
			}
			catch (Exception E)
			{
				Log.Error(E, "ERROR. Disconnect.");
			}
		}
		protected BayeuxClient CreateBayeuxClient(SiteClient ASite)
		{
			try
			{
				lock (this)
				{
					//check if this corp already has a client created for it....
					foreach (KeyValuePair<int, SiteClient> sc in SiteList)
					{
						if (sc.Value.CorpID == ASite.CorpID)
						{
							if (sc.Value.Client != null)
							{
								//if so just return that and assign it to site
								ASite.Client = sc.Value.Client;
								return sc.Value.Client;
							}
						}
					}

					var options = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
				{
					{ ClientTransport.MAX_NETWORK_DELAY_OPTION, 120000} //2 minutes


				};

					Int32 WaitInterval = 60000; //1 minute
					IdealLongPollingTransport2 LTransport;
					LTransport = new IdealLongPollingTransport2(options);
					LTransport.OnConnectionError += IdealLongPollingTransport_OnConnectionError;         //TODO: need to unassign this handler on destruction
					BayeuxClient client = new BayeuxClient(ASite.PubSubUrl, new List<ClientTransport>() { LTransport });
					client.handshake();
					if (client.waitFor(WaitInterval, new List<BayeuxClient.State>() { BayeuxClient.State.CONNECTED }) == BayeuxClient.State.CONNECTED)
					{
						//successful connection
						Log.Info("   Successful connection to " + ASite.PubSubUrl + "  Client ID: " + client.Id);
						client.addExtension(new IdealAPIExtension(new VrStoreSetup(ASite)));
						//assign this client to all sites for this same corp....
						foreach (KeyValuePair<int, SiteClient> sc in SiteList)
						{
							if (sc.Value.CorpID == ASite.CorpID)
							{
								sc.Value.Client = client;
							}
						}
					}
					else
					{
						throw new Exception("   Unable to connect to " + ASite.PubSubUrl + " in " + WaitInterval + " milliseconds.");
					}
					return client;

				}
			}
			catch (Exception E)
			{
				CheckForConnectionError(E);
				throw;
			}

		}
		protected BayeuxClient CreateClient(SiteClient site, bool ReuseExistingBayeuxClient = true)
		{
			if ((site.Client != null) && (ReuseExistingBayeuxClient)) return site.Client;
			BayeuxClient client = CreateBayeuxClient(site);
			site.Client = client;
			return client;
		}
		private bool IdealLongPollingTransport_OnConnectionError(Exception E)
		{
			Log.Error(E, "Connection Error. Will Reset Connection. Current action most likely unsuccessful. ");
			ResetPubSubConnection();
			Log.LastAutoRecoverCause = E.Message;
			return false;
		}

		Boolean FInResetPubSubConnection = false;
		Boolean FRetryReset = false;
		private readonly object ResetLock = new object();
		Task ResetTask = null;

		public void ResetPubSubConnection()
		{
			FRetryReset = true;
			if (ResetTask == null)
			{
				ResetTask = new Task(() =>
				   {
					   while (FRetryReset)
					   {
						   try
						   {
							   if (!FInResetPubSubConnection)
							   {
								   Log.Info("Trying reset...");
								   DoResetPubSubConnection();
							   }
						   }
						   catch (Exception E)
						   {
							   FRetryReset = true;
							   Log.Error(E);
						   }
						   Thread.Sleep(1000);
					   }
					   Log.Debug("ResetPubSubConnection succeeded without error.");
					   ResetTask = null;
				   });
				ResetTask.Start();
			}
		}

		private void DoResetPubSubConnection()
		{
			if (!FInResetPubSubConnection)
			{
				FInResetPubSubConnection = true;
				try
				{
					FRetryReset = false;
					try
					{
						try
						{
							Log.Info("[ResetConnection]");
							if (DateTime.UtcNow < FLastReset.AddSeconds(RESET_DELAY)) Log.Info("...Waiting");
							while (DateTime.UtcNow < FLastReset.AddSeconds(RESET_DELAY))
							{
								//too soon for reset of connection, wait a few seconds...

							}
							FLastReset = DateTime.UtcNow;
							Disconnect();
							if (Task_SubscribeToChannels != null)
							{
								Task_SubscribeToChannels.Wait(10000);
								//Task_SubscribeToChannels.Dispose();
								Task_SubscribeToChannels = null;
							}
							SiteList.Clear();
							SubscribedChannels.Clear();

						}
						catch (Exception E)
						{
							Log.Error(E, "[ResetConnection] Cleaning up");
							throw;
						}
						try
						{
							ConnectAndSubscribe(FSiteID);
							FLastReset = DateTime.UtcNow;
						}
						catch (Exception E)
						{
							Log.Error(E, "[ResetConnection] Resurrecting");
							throw;
						}
						Log.Info("[ResetConnection] Done. ");
						Log.LastTimeAutoRecovered = DateTime.Now;
					}
					catch (Exception E)
					{
						FRetryReset = true;
						Log.Info("[ResetConnection] Errored out. " + E.Message);
					}
				}
				finally
				{
					FInResetPubSubConnection = false;

				}


			}
			else
			{
				//Log.Info("[ResetConnection] was already activated.");
				//FRetryReset = true;
			}
		}

		private static int g_PubSyncId;
		private string FSiteID;

		public VrStorePubSubClass(string ASiteID = "")
		{

			FLastReset = DateTime.MinValue;
			SiteList = new ConcurrentDictionary<int, SiteClient>();
			SubscribedChannels = new ConcurrentDictionary<string, BayeuxClient>();
			Boolean NeedRetry = true;
			while (NeedRetry)
			{
				try
				{
					//TODO: this demo is just a client not the store itself, so does not need to ConnectAnd Subscribe
					//ConnectAndSubscribe(ASiteID);
					NeedRetry = false;
				}
				catch (Exception E)
				{
					NeedRetry = true;
					Log.Error(E, "ERROR. VrStorePubSubClass constructor..");
					try
					{
						Disconnect();
					}
					catch (Exception e)
					{
						Log.Error(E);
					}
					Thread.Sleep(1000);
				}
			}
		}

		private void ConnectAndSubscribe(string ASiteID)
		{
			//OnConnectionError += IdealLongPollingTransport_OnConnectionError;
			FSiteID = ASiteID;
			//determine if we are in VrStorePubSubClient or in a specific site....
			IsSingleSite = (ASiteID != "");

			try
			{
				Log.Debug("[ConnectAndSAubscribe] SiteID: " + ASiteID);

				SiteClient Site = GenerateNewSiteClient();

				if (SiteList.TryAdd(Convert.ToInt32(Site.SiteID), Site))
				{
					Log.Info("[VrStorePubSubClass] Added Site to Site dictionary. SiteID: " + Site.SiteID + "  CorpID=" + DEMO_CORPID + "  SiteID=" + DEMO_SITEID);
				}

				StringBuilder sb = new StringBuilder();
				sb.AppendLine("-------------------");
				sb.AppendLine("   Found " + SiteList.Count + " Sites.");
				foreach (KeyValuePair<int, SiteClient> p in SiteList)
				{
					sb.AppendLine("   SiteID: " + p.Value.SiteID + "   Corp ID: " + p.Value.CorpID + "   " + p.Value.Description + "   Channel: " + p.Value.Channel + " at " + p.Value.PubSubUrl);

				}
				sb.AppendLine("-------------------");
				Log.Info(sb.ToString());
				if (IsSingleSite)
				{
					Log.Info("  Single Site, so no subscriptions.");
				}
				else
				{
					Log.Info("  Subscribing to public channel...");
					Task_SubscribeToChannels = new Task(() => SubscribeToChannels());
					Task_SubscribeToChannels.Start();
					Task_SubscribeToChannels.Wait();
				}
			}
			catch (Exception E)
			{
				Log.Error(E, "Error trying to connect and subscribe for site " + ASiteID);
			}
		}

		private static SiteClient GenerateNewSiteClient()
		{
			//TODO: create and add SiteClients to the SiteList for each site in this corp
			//for this demo there is just one
			SiteClient Site = new SiteClient();
			Site.CorpID = DEMO_CORPID;
			Site.GroupID = DEMO_OLAFID;
			Site.Channel = "/" + VrStoreHelper.VrStore_api + "/" + Site.GroupID + "/" + DEMO_SITEID;// + "/";
			Site.SiteID = DEMO_SITEID;
			Site.token = DEMO_TOKEN;
			Site.PrivateKey = DEMO_PRIVATEKEY;
			Site.PubSubUrl = DEMO_PUBSUBURL;
			Site.Description = DEMO_CORPDESCRIPTION + " at " + DEMO_SITENAME;
			Site.Listener = null;
			return Site;
		}

		public static string CreateReturnChannel(SiteClient site)
		{
			string[] cParts = site.Channel.Split('/');
			string ReturnChannel = "/" + cParts[1] + "/" + cParts[2] + "/" + site.Client.Id + "/return"; // + MinimalizeGUID(Guid.NewGuid().ToString());
			return ReturnChannel;
		}

		private static string MinimalizeGUID(string v)
		{
			string res = "";
			foreach(char c in v)
			{
				if (c == '{') continue;
				if (c == '}') continue;
				if (c == '-') continue;
				res += c;
			}
			return res.ToLower();
		}

		static public string NormalizePhoneNumber(string v)
		{
			string LRes = "";
			LRes = v.Replace("+1", "");
			LRes = LRes.Replace("+", "");
			LRes = LRes.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");

			return LRes;
		}

	}

}
