using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Cometd.Bayeux;
using Cometd.Common;
using Cometd.Client.Transport;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Net;
using Demo.Logging;

namespace Ideal.LongPollingTransport
{
	/// <version>  $Revision$ $Date: 2010-10-19 12:35:37 +0200 (Tue, 19 Oct 2010) $
	/// </version>
	public delegate bool ConnectionErrorHandler2(Exception E);  //return true if error handled and so should not abort the code that follows it

	public class IdealLongPollingTransport2 : HttpClientTransport
    {

		private List<TransportExchange> _exchanges = new List<TransportExchange>();
        //private bool _aborted;
        private bool _appendMessageType;

		public event ConnectionErrorHandler2 OnConnectionError = null;

		public bool CheckForConnectionError(Exception E)
		{
			bool LConnectionErrorOccurred = false;
			if (E == null) return (true);
			LConnectionErrorOccurred = (
				E.Message.Contains("(503) Server Unavailable.")
				|| E.Message.Contains("(502) Bad Gateway.")
				|| E.Message.Contains("(504)")
				|| E.Message.Contains("(505)")
				|| E.Message.Contains("(500)")
				|| E.Message.Contains("(501)")
				|| E.Message.Contains("Unable to connect")
                || E.Message.ToLower().Contains("connection was closed")
                || E.Message.ToLower().Contains("unknown client")
                || E.Message.ToLower().Contains("request was aborted")
                );
			if (LConnectionErrorOccurred && (OnConnectionError != null))
			{
				return OnConnectionError(E);
			}
			return true;
		}

		public IdealLongPollingTransport2(IDictionary<String, Object> options)
            : base("long-polling", options)
        {
        }

		public IList<IMessage> ToListOfIMessage(IList<IMutableMessage> M)
		{
			List<IMessage> R = new List<IMessage>();
			foreach (IMutableMessage m in M)
			{
				R.Add((IMessage)m);
			}
			return R;
		}

		public IList<IDictionary<String, Object>> ToListOfDictionary(IList<IMutableMessage> M)
		{
			IList<IDictionary<String, Object>> R = new List<IDictionary<String, Object>>();

			foreach (IMutableMessage m in M)
			{
				R.Add((IDictionary<String, Object>)m);
			}
			return R;
		}

		public override bool accept(String bayeuxVersion)
        {
            return true;
        }

        public override void init()
        {
            base.init();
            //_aborted = false;
            Regex uriRegex = new Regex("(^https?://(([^:/\\?#]+)(:(\\d+))?))?([^\\?#]*)(.*)?");
            Match uriMatch = uriRegex.Match(getURL());
            if (uriMatch.Success)
            {
                String afterPath = uriMatch.Groups[7].ToString();
                _appendMessageType = afterPath == null || afterPath.Trim().Length == 0;
            }
        }

        public override void abort()
        {
            //_aborted = true;
            lock (this)
            {
                foreach (TransportExchange exchange in _exchanges)
                    exchange.Abort();

                _exchanges.Clear();
            }
        }

        public override void reset()
        {
        }

        // Fix for not running more than two simultaneous requests:
        public class LongPollingRequest
        {
            ITransportListener listener;
            IList<IMutableMessage> messages;
            HttpWebRequest request;
			IdealLongPollingTransport2 LParent;
            public TransportExchange exchange;

            public LongPollingRequest(ITransportListener _listener, IList<IMutableMessage> _messages,
                    HttpWebRequest _request, IdealLongPollingTransport2 AParent)
            {
                listener = _listener;
                messages = _messages;
                request = _request;
				LParent = AParent;
			}

            public void send()
            {
				try
                {
                    request.BeginGetRequestStream(new AsyncCallback(LParent.GetRequestStreamCallback), exchange);
				}
                catch (Exception E)
                {
                    exchange.Dispose();
					Log.Error(E, "Error sending LongPollingRequest. ");
                }
            }
        }

        private ManualResetEvent ready = new ManualResetEvent(true);
        private List<LongPollingRequest> transportQueue = new List<LongPollingRequest>();
        private HashSet<LongPollingRequest> transmissions = new HashSet<LongPollingRequest>();

        private void performNextRequest()
        {
			bool ok = false;
            LongPollingRequest nextRequest = null;
			int MaxSimultaneousRequests = 2;

            lock (this)
            {
                if (transportQueue.Count > 0 && transmissions.Count <= MaxSimultaneousRequests-1)
                {
                    ok = true;
                    nextRequest = transportQueue[0];
                    transportQueue.Remove(nextRequest);
                    transmissions.Add(nextRequest);
                }
            }

            while (ok && nextRequest != null)
            {
				nextRequest.send();

				lock (this)
				{
					if (transportQueue.Count > 0 && transmissions.Count <= MaxSimultaneousRequests-1)
					{
						ok = true;
						nextRequest = transportQueue[0];
						transportQueue.Remove(nextRequest);
						transmissions.Add(nextRequest);
					}
					else
					{
						ok = false;
						nextRequest = null;
					}
				}

			}
		}

        public void addRequest(LongPollingRequest request)
        {
			int LCount = 0;
			int LSendingCount = 0;
			lock (this)
            {
				transportQueue.Add(request);
				LCount = transportQueue.Count;
				LSendingCount = transmissions.Count;
			}
			performNextRequest();
		}

        public void removeRequest(LongPollingRequest request)
        {
            lock (this)
            {
                transmissions.Remove(request);
            }

            performNextRequest();
        }

        public override void send(ITransportListener listener, IList<IMutableMessage> messages)
        {
			try
			{
				//Console.WriteLine();
				//Console.WriteLine("send({0} message(s))", messages.Count);
				String url = getURL();

				if (_appendMessageType && messages.Count == 1 && messages[0].Meta)
				{
					String type = messages[0].Channel.Substring(Channel_Fields.META.Length);
					if (url.EndsWith("/"))
						url = url.Substring(0, url.Length - 1);
					url += type;
				}

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.Method = "POST";
				request.ContentType = "application/json;charset=UTF-8";

				LongPollingRequest longPollingRequest = new LongPollingRequest(listener, messages, request, this);

				JavaScriptSerializer jsonParser = new JavaScriptSerializer();
				String content = jsonParser.Serialize(ToListOfDictionary(messages));


				TransportExchange exchange = new TransportExchange(this, listener, messages, longPollingRequest);
				exchange.content = content;
				exchange.request = request;
				lock (this)
				{
					_exchanges.Add(exchange);
				}

				longPollingRequest.exchange = exchange;
				addRequest(longPollingRequest);
			}
			catch (Exception E)
			{
				Log.Error(E, "[IdealLongPollingTransport.send]");
				if(!CheckForConnectionError(E)) throw;
			}
		}

        public override bool isSending
        {
            get
            {
                lock (this)
                {
                    if (transportQueue.Count > 0)
                        return true;

                    foreach (var transmission in transmissions)
                        if (transmission.exchange.isSending)
                            return true;

                    return false;
                }
            }
        }

        // From http://msdn.microsoft.com/en-us/library/system.net.httpwebrequest.begingetrequeststream.aspx
        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
			TransportExchange exchange = (TransportExchange)asynchronousResult.AsyncState;

            try
            {
				// End the operation
                using (Stream postStream = exchange.request.EndGetRequestStream(asynchronousResult))
                {
                    // Convert the string into a byte array.
                    byte[] byteArray = Encoding.UTF8.GetBytes(exchange.content);
                    //Console.WriteLine("Sending message(s): {0}", exchange.content);

                    // Write to the request stream.
                    postStream.Write(byteArray, 0, exchange.content.Length);
                    postStream.Close();
                }
                Log.LastTimeLongPollingMessageSent = DateTime.Now;
                // Start the asynchronous operation to get the response
                exchange.listener.onSending(ToListOfIMessage(exchange.messages));
                IAsyncResult result = (IAsyncResult)exchange.request.BeginGetResponse(new AsyncCallback(GetResponseCallback), exchange);

                long timeout = 120000;
                ThreadPool.RegisterWaitForSingleObject(result.AsyncWaitHandle, new WaitOrTimerCallback(TimeoutCallback), exchange, timeout, true);
                exchange.isSending = false;
			}
            catch (Exception e)
            {
				Log.Error(e, "[Ideal.LongPollingTransport2.GetRequestStreamCallback]");
				if (exchange.request != null)
                {
                    try
                    {
                        exchange.request.Abort();
                    }
                    catch (ObjectDisposedException) { }
                }
                //exchange.listener.onException(e, ToListOfIMessage(exchange.messages));
				exchange.Dispose();
                CheckForConnectionError(e);
            }
		}

        private void GetResponseCallback(IAsyncResult asynchronousResult)
        {
			TransportExchange exchange = (TransportExchange)asynchronousResult.AsyncState;

            try
            {
                // End the operation
                string responseString;
                using (HttpWebResponse response = (HttpWebResponse)exchange.request.EndGetResponse(asynchronousResult))
                {
                    using (Stream streamResponse = response.GetResponseStream())
                    {
                        using (StreamReader streamRead = new StreamReader(streamResponse))
                            responseString = streamRead.ReadToEnd();
                    }
                    //Console.WriteLine("Received message(s): {0}", responseString);

                    if (response.Cookies != null)
                        foreach (Cookie cookie in response.Cookies)
                            exchange.AddCookie(cookie);
                    response.Close();
                }
                exchange.messages = DictionaryMessage.parseMessages(responseString);
                
                exchange.listener.onMessages(exchange.messages);
                exchange.Dispose();
                Log.LastTimeLongPollingMessageRecieved = DateTime.Now;
            }
            catch (Exception e)
            {
				Log.Error(e, "[IdealLongPollingTransport.GetResponseCallback]");
				//exchange.listener.onException(e, ToListOfIMessage(exchange.messages));
				if (exchange.request != null)
				{
					try
					{
						exchange.request.Abort();
					}
					catch (ObjectDisposedException) { }
				}
				exchange.Dispose();
                CheckForConnectionError(e);
            }
        }

        // From http://msdn.microsoft.com/en-us/library/system.net.httpwebrequest.begingetresponse.aspx
        // Abort the request if the timer fires.
        private void TimeoutCallback(object state, bool timedOut)
        {
			if (timedOut)
            {
                TransportExchange exchange = state as TransportExchange;
				Log.Info("   ...Timeout. aborting request with this content: "+exchange.content);

				if (exchange.request != null) exchange.request.Abort();
                exchange.Dispose();
			}
		}


		public class TransportExchange
        {
            private IdealLongPollingTransport2 parent;
            public String content;
            public HttpWebRequest request;
            public ITransportListener listener;
            public IList<IMutableMessage> messages;
            public LongPollingRequest lprequest;
            public bool isSending;

            public TransportExchange(IdealLongPollingTransport2 _parent, ITransportListener _listener, IList<IMutableMessage> _messages,
                    LongPollingRequest _lprequest)
            {
                parent = _parent;
                listener = _listener;
                messages = _messages;
                request = null;
                lprequest = _lprequest;
                isSending = true;
            }

            public void AddCookie(Cookie cookie)
            {
                parent.addCookie(cookie);
            }

            public void Dispose()
            {
                parent.removeRequest(lprequest);
                lock (parent)
                    parent._exchanges.Remove(this);
            }

            public void Abort()
            {
                if (request != null) request.Abort();
            }
        }
    }
}
