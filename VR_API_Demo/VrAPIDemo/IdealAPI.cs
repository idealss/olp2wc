using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using Cometd.Bayeux;
using Cometd.Bayeux.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Demo.Logging;
using Cometd.Client;

namespace CometD.Client.Ideal.VrStore.API
{

	public class SiteClient
	{
		public string token { get; internal set; }
		public string PrivateKey { get; internal set; }
		public string GroupID { get; internal set; }
		public string Channel { get; internal set; }
		public string CorpID { get; internal set; }
		public string SiteID { get; internal set; }
		public string PubSubUrl { get; internal set; }
		public string Description { get; internal set; }
		public IMessageListener Listener { get; internal set; }
		public BayeuxClient Client { get; internal set; }
	}

	public static class VrStoreHelper
    {
		//Here we define all the string constants used as message types for this API
		public const string VrStore_api = "vr_store";

		public const string VrStore_signup = "signup";  //Message sent by a client to request a new OLP username and password be established for known account information.

		public const string VrStore_reset_password = "reset_password";  //Message sent by a client to request a new temporary password be sent to the user�s known email address.

		public const string VrStore_change_password = "change_password";  //Message sent by a client to initiate a change password request using the current known password and a new password.

		public const string VrStore_authenticate = "authenticate";  //Message sent by a client to log in a user with a known username and password.Authentication is completed using the OLP username and password as stored at the store.The authentication will also return a customerID and token that must accompany any messages involving the authenticated customer (only applies to third_party sources).

		public const string VrStore_logout = "logout";  //Message sent by a client to invalidate the known user context of the communication.

		public const string VrStore_update_account = "update_account";  //Message sent by a client to request information be updated on the user�s account. This includes username, password, and e-mail address.

		public const string VrStore_list_accounts = "list_accounts";  //Message sent by a client to request a list of accounts and available payment options.

		public const string VrStore_site_information = "site_information";  //Message sent by a client to learn key information about the store they are connected with.  Information returned includes address and phone number.This will also contain some settings used to control the behavior of the site.

		public const string VrStore_customer_information = "customer_information";  //Message sent by a client to return known information about the currently logged in customer.

		public const string VrStore_list_stored_cards = "list_stored_cards";  //Message sent by a client to return a list of stored cards currently on the customer account at the store.

		public const string VrStore_pay_stored_card = "pay_stored_card";  //Message sent by a client process either a stored card or payeezy generated token and record the payment.  See list_stored_cards to retrieve the customer�s stored cards.

		public const string VrStore_update_customer_information = "update_customer_information";  //(Future message) Information must be validated at the store before information update is complete to protect against bogus information being entered by suspect users.

		public const string VrStore_payment_history = "payment_history";  //Lists the last 5 payments made by the customer

		public const string VrStore_list_categories = "list_categories";  //List of departments and product codes.  Can be filtered to show only available, new and used.

		public const string VrStore_list_models = "list_models";  //List of models filtered by a specific product code.Can also be filtered to show only available, new and used.

		public const string VrStore_list_inventory = "list_inventory";  //List of items.Must be filtered by either model or specific item id.

		public const string VrStore_collect_deposit = "collect_deposit";  //Accepts an amount, person information (crmpeopleid or custid could be optional if known) and card information required to process and record for a deposit amount(recorded with customer�s floating fund).

		public const string VrStore_club_point_customer = "club_point_customer";  //Used to validate customer id and generate a token to be used for the other club point messages

		public const string VrStore_club_point_balance = "club_point_balance";  //accept customer id and token and returns the point balance for the customer

		public const string VrStore_club_point_history = "club_point_history";  //accept customer id and token and returns the point transaction history for the customer

		public const string VrStore_club_point_transaction = "club_point_transaction";  //accepts customer id, token transaction type and amount and records either an addition of points or reduction of points for the customer.
		
		public const string VrStore_collect_deposit_verify = "collect_deposit_verify";  //Validates if a previous collect_deposit call was successful. Must supply an apiID with the collect_deposit call and used here to validate.

		public static string Base64Encode(string plainText)
		{
			var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			return Convert.ToBase64String(plainTextBytes);
		}

		public static string getHashSha256(byte[] text)
		{
			var hashstring = new SHA256Managed();
			var hash = hashstring.ComputeHash(text);
			var hashString = string.Empty;
			foreach (var x in hash)
				hashString += string.Format("{0:x2}", x);
			return hashString.ToLower();
		}

		public static Dictionary<string, object> deserializeToDictionary(string jo)
		{
			var serializer = new JsonSerializerSettings();
			serializer.DateFormatHandling = 0;

			var values = JsonConvert.DeserializeObject<Dictionary<string, object>>(jo, serializer);
			var values2 = new Dictionary<string, object>();
			foreach (var d in values)
				if (d.Value is JObject)
					values2.Add(d.Key, deserializeToDictionary(d.Value.ToString()));
				else
					values2.Add(d.Key,
						d.Value is DateTime ? ((DateTime)d.Value).ToString("s", CultureInfo.InvariantCulture) : d.Value);
			return values2;
		}

		public static byte[] getSalt()
		{
			var random = new RNGCryptoServiceProvider();

			// Maximum length of salt
			var max_length = 32;

			// Empty salt array
			var salt = new byte[max_length];

			// Build the random bytes
			random.GetNonZeroBytes(salt);

			// Return the string encoded salt
			return salt;
		}

		public static string MakeSignature(byte[] Salt, string Data, string PrivateKey)
		{
			var step1 = getHashSha256(Encoding.UTF8.GetBytes(Data));
			var step2 = getHashSha256(Encoding.UTF8.GetBytes(step1 + PrivateKey));
			return getHashSha256(Combine(Encoding.UTF8.GetBytes(step2), Salt));
		}

		public static byte[] Combine(byte[] first, byte[] second)
		{
			var ret = new byte[first.Length + second.Length];
			Buffer.BlockCopy(first, 0, ret, 0, first.Length);
			Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
			return ret;
		}

	}

    public class VrStoreSetup
    {
        public string ConnectionString;
        public string token;
        public string PrivateKey;

		public string GroupID { get; internal set; }

		public VrStoreSetup(SiteClient site)
		{
			ConnectionString = "";
			GroupID = site.GroupID;
			PrivateKey = site.PrivateKey;
			token = site.token;
		}
    }

    /// <summary>
    ///     This client extension will add the client timestamp into each messages
    ///     that will be sent to the server.
    /// </summary>
    [Serializable]
    public sealed class IdealAPIExtension : IExtension, IEquatable<IdealAPIExtension>
    {
		VrStoreSetup setup = null;

		public IdealAPIExtension(VrStoreSetup ASetup): base()
		{
			setup = ASetup;
		}

		private IdealAPIExtension()
		{
			throw new Exception("NOT IMPLEMENTED: default constructor for IdealAPIExtension");
		}

		private void AddAPI(IMutableMessage message)
        {
            lock (message)
            {
				string json = "";
				try
				{
					var env = new JObject();
					json = JsonConvert.SerializeObject(message, Formatting.Indented);
					var msg = JObject.Parse(json);
					var Salt = VrStoreHelper.getSalt();


					if (message.Channel == "/meta/subscribe")
					{
						env.Add("api", "vr_store");
						env.Add("token", setup.token);
						env.Add("salt", Salt);
						env.Add("signature", VrStoreHelper.MakeSignature(Salt, json, setup.PrivateKey));
						env.Add("message", message.Channel);
						env.Add("data", VrStoreHelper.Base64Encode(json));
						message[Cometd.Bayeux.Message_Fields.EXT_FIELD] = VrStoreHelper.deserializeToDictionary(env.ToString());
					}
					else
					{
						if (msg["data"] != null)
						{
							env.Add("api", "vr_store");

							if ((msg["data"] != null) & (msg["data"]["v1"] != null))
								env.Add("message", (string)msg["data"]["message"]);
							else
								throw new Exception(DateTime.Now +
													" | [VrStore][API]Packet missing v1 section or v1.message element. \r\n" +
													(string)msg["data"] + "\n");

							env.Add("token", setup.token);
							env.Add("salt", Salt);
							env.Add("signature", VrStoreHelper.MakeSignature(Salt, json, setup.PrivateKey));
							env.Add("data", VrStoreHelper.Base64Encode(json));
							message[Cometd.Bayeux.Message_Fields.EXT_FIELD] = VrStoreHelper.deserializeToDictionary(env.ToString());
							message.Data = new object();
						}
					}
					//if(env.ToString() != "{}") Log.Info("IdealAPIExtension Bayeaux Message Field [EXT_FIELD]: " + env.ToString());
				}
				catch (Exception E)
				{
					Log.Error(E, "Error in AddAPI. " + json);
				}
            }
        }

        #region IExtension Members

        /// <summary>
        ///     Callback method invoked every time a normal message is being sent.
        /// </summary>
        /// <returns>Always true.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="message" /> is null.</exception>
        public bool send(IClientSession session, IMutableMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            AddAPI(message);
			StringBuilder sb = new StringBuilder();
			sb.AppendLine();
			sb.AppendLine(">>>>>>>>>>>>>>>>>------>Session: " + session.Id + " Sending RegularMessage from IdealaPI: ");
			sb.AppendLine(message.JSON);
			sb.AppendLine(">>>>>>>>>>>>>>>>>-END-->");
			Log.Debug(sb.ToString());
            Log.LastTimeRegularMessageSent = DateTime.Now;
			return true;
        }

        /// <summary>
        ///     Callback method invoked every time a meta message is being sent.
        /// </summary>
        /// <returns>Always true.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="message" /> is null.</exception>
        public bool sendMeta(IClientSession session, IMutableMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            AddAPI(message);
			StringBuilder sb = new StringBuilder();
			sb.AppendLine();
			sb.AppendLine(">>>>>>>>>>>>>>>>>-META----->Session: " + session.Id + " Sending MetaMessage from IdealaPI: ");
			sb.AppendLine( message.JSON) ;
			sb.AppendLine(">>>>>>>>>>>>>>>>>-END META->");
			Log.Info(sb.ToString());
            Log.LastTimeMetaMessageSent = DateTime.Now;
            return true;
        }

		public bool rcv(IClientSession session, IMutableMessage message)
		{
			string json = JsonConvert.SerializeObject(message, Formatting.Indented);
			StringBuilder sb = new StringBuilder();
			sb.AppendLine();
			sb.AppendLine("<<<<<<<<<<<<<<------<Session: " + session.Id + " Receiving RegularMessage from IdealaPI: ");
			sb.AppendLine(message.JSON);
			sb.AppendLine("<<<<<<<<<<<<<<-END--<");
			Log.Debug(sb.ToString());
            Log.LastTimeRegularMessageReceived = DateTime.Now;
			return true;
		}

		public bool rcvMeta(IClientSession session, IMutableMessage message)
		{
			string json = JsonConvert.SerializeObject(message, Formatting.Indented);
			StringBuilder sb = new StringBuilder();
			sb.AppendLine();
			sb.AppendLine("<<<<<<<<<<<<<<-META------<Session: " + session.Id + " Receiving MetaMessage via IdealaPI: ");
			sb.AppendLine(message.JSON);
			sb.AppendLine("<<<<<<<<<<<<<<-END META--<");
			if (message.JSON.ToString().ToLower().Contains("error"))
			{
				Log.Warn("Error received via META message in client session: "+ sb.ToString());
                if (message.JSON.ToString().ToLower().Contains("subscri"))
                {
                    //need reset if error subscribing....

                }

            }
			else
			{
                Log.Debug(sb.ToString());
			}
            Log.LastTimeMetaMessageReceived = DateTime.Now;
            return true;
		}

		#endregion

		#region IEquatable Members

		/// <summary>
		///     Returns a value indicating whether this extension
		///     is equal to another <see cref="IExtension" /> object.
		/// </summary>
		public bool Equals(IExtension other)
        {
            return null == other ? false : other is IdealAPIExtension;
        }

        /// <summary>
        ///     Returns a value indicating whether this extension
        ///     is equal to another <see cref="IdealAPIExtension" /> object.
        /// </summary>
        public bool Equals(IdealAPIExtension other)
        {
            return null != other;
        }

		/// <summary>
		///     Returns a value indicating whether this extension is equal to a specified object.
		/// </summary>
		override public bool Equals(object obj)
        {
            return null == obj ? false : obj is IdealAPIExtension;
        }

        /// <summary>
        ///     Returns the hash code for this extension.
        /// </summary>
        override public int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
    }
}
