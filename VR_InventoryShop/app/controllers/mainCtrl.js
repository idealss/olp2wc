﻿angular.module('app')
 .controller('mainCtrl', ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state) {

     $rootScope.messageReceived = { data: { message: '' } };
     $rootScope.isSubscribe = false;
     $rootScope.ErrorMessage = "";
     $rootScope.EmaiAddress = "";
     $rootScope.phoneNo = "";
     $rootScope.storeINFO = "";


     try {
         // check api response 
         $rootScope.message = "";
         $scope.$watch(function () {
             return $rootScope.messageReceived;
         }, function (newVal, oldVal) {

             if (newVal.data.message === 'connect') {
                 $rootScope.isSubscribe = false;
                 $rootScope.message = "";
                 $rootScope.Loaded = true;
             }

             //check site_information api method response
             if (newVal.data.message === 'site_information') {
                 $rootScope.message = "";
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage.checkStoreInformation = "have value";
                     sessionStorage.currentTimeStamp = newVal.data.v1.currentTimeStamp;
                     $rootScope.storeINFO = newVal.data.v1.address + " " + newVal.data.v1.city + " " + " " + newVal.data.v1.state + " " + newVal.data.v1.zip;
                     $rootScope.phoneNo = newVal.data.v1.phone;
                     $rootScope.EmaiAddress = newVal.data.v1.customerServiceEmai;
                 }
                 else {
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $rootScope.ShowLoader = false;

                 }
             }

             //check list_categories api method response
             if (newVal.data.message === 'list_categories') {
                 $rootScope.isLogin = true;
                 $rootScope.message = "";
                 if (newVal.data.v1.errorDescription != 'Successful') {
                     sessionStorage['list_models'] = '';
                     $rootScope.errorCount++;
                     $rootScope.ShowLoader = false;

                     $rootScope.message = newVal.data.v1.errorDescription;
                 }
                 else if (newVal.data.v1.errorDescription === 'Successful') {

                     sessionStorage['list_categories'] = JSON.stringify($rootScope.messageReceived);

                     $state.reload('start');


                 }
                 else {
                     $rootScope.message = newVal.data.v1.errorDescription;
                     $rootScope.errorCount = 0;
                     $rootScope.ShowLoader = false;

                 }
             }

             //check list_models api method response
             if (newVal.data.message === 'list_models') {
                 $rootScope.message = "";
                 $rootScope.ShowLoader = false;
                 if (newVal.data.v1.errorDescription === 'Successful') {

                     sessionStorage['list_inventory'] = "";

                     sessionStorage['list_models'] = JSON.stringify($rootScope.messageReceived);


                     if ($state.current.name == 'start.search' || $state.current.name == 'start.search.item') {
                         $state.reload('start.search');
                     }
                     else {
                         $state.go('start.search');
                     }

                 }
                 else {
                     $rootScope.ShowLoader = false;
                     $rootScope.message = newVal.data.v1.errorDescription;
                 }

             }
             //check list_inventory api method response
             if (newVal.data.message === 'list_inventory') {
                 if (newVal.data.v1.errorDescription === 'Successful') {
                     sessionStorage['list_inventory'] = JSON.stringify($rootScope.messageReceived);



                     if ($state.current.name == 'start.search.item') {
                         $state.reload('start.search.item');
                     }
                     else {
                         $state.go('start.search.item');
                     }

                 }
                 else {

                     $rootScope.message = newVal.data.v1.errorDescription;
                 }

             }
         });
     }
     catch (e) {
         console.log(ew);
     }

 }]);