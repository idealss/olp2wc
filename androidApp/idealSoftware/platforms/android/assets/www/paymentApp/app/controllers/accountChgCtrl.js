﻿angular.module("app")
    .controller("accountSubmit", ["$scope", "$rootScope", "idealsApi", function($scope, $rootScope, idealsApi) {

        // logout click
        try {
            $rootScope.message = "";
            $scope.userFLName = sessionStorage.userFLName;
            $scope.paymentHistory = function() {
                idealsApi.payment_historyReq();
            }
        } catch (e) {
            console.log(e);
        }
    }]);