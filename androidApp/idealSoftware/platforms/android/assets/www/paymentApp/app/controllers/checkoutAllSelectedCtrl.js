﻿angular.module("app")
//      .controller("checkoutAllSelectedCtrl", ["$scope", "$rootScope", "idealsApi", "$location", "$cookieStore", "Apicookies", function($scope, $rootScope, idealsApi, $location, $cookieStore, Apicookies) {
        .controller("checkoutAllSelectedCtrl", ["$scope", "$rootScope", "idealsApi", "$location", "$cookieStore", "configDetailsProvider", function($scope, $rootScope, idealsApi, $location, $cookieStore, configDetailsProvider) {


        try {
                $('#hamburger-inner-navbar').click(function () {
                        $("#inner-navbar").toggleClass("in");
                });
            $scope.userFLName = sessionStorage.userFLName;
            var listCards = JSON.parse(sessionStorage["list_cards"] || "{}");
            $rootScope.message = "";

            // check list_cards api method data
            if (listCards) {
                var count = 0;

                $scope.payment = JSON.parse(sessionStorage["paymentAmt"] || '{}');
                var amountOfPayment = $scope.payment.amountOfPayment;
                var itemDescription = $scope.payment.itemDescription;
                var accountType = $scope.payment.accountType;
                var accountID = $scope.payment.accountID;
                var nextDueDate = ($scope.payment.nextDueDate);

                for (var index = 0; index < $scope.payment.length; index++) {
                    count = count + parseFloat($scope.payment[index].amountOfPayment);
                };

                $scope.total = "$" + (count).toFixed(2);

                var json = listCards;
                var cards = json.data.v1.cards;

                // check visibility of card payment and enter card information button
                if (cards.length > 0) {
                    sessionStorage["cards"] = JSON.stringify(cards);
                    $scope.IsVisible1 = true;
                    $scope.IsVisible2 = true;
                } else {
                    sessionStorage["cards"] = null;
                    $scope.IsVisible1 = true;
                    $scope.IsVisible2 = false;
                }
            };

            //click on enter new card button and redirect to checkout page
            $scope.clickNewCardInfo = function() {
                $rootScope.disable = false;
                $location.path("/checkOut");
            };

             var cookieWObject= configDetailsProvider.apiConnect[0];
             $scope.PayOptionSubmit = function () {
//                 Apicookies.writePayezzyCookie();
//                 Apicookies.writeApiCookie();
//                 var cookieWObject = ($cookieStore.get('payeezy_auth'));

                 if (cookieWObject)
                 {
                     $rootScope.disable = false;
                     if (cookieWObject.processor_type == 0) {
                        // pay by payeezy
                         $rootScope.checkProcessor = 'payeezy';
                         $location.path('/checkOut');
                     }
                     else if (cookieWObject.processor_type == 1) {
                         // pay by cardconnect
                         $rootScope.checkProcessor = 'cardconnect';
                         $location.path('/checkOut');
                     }

                 }
                 else {
                     return false;
                 }
             };

            //click on saved card button and redirect to checkout page
            $scope.clickSavedCard = function() {

                $rootScope.disable = true;
                $location.path("/checkOut");
            };

            $scope.paymentHistory = function() {
                idealsApi.payment_historyReq();
            };
        } catch (e) {
            console.log(e);
        }
    }]);