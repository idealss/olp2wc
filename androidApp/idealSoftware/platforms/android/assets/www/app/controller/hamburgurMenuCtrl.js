﻿angular.module("app")
 .controller("MenuCtrl", ["$scope", "$rootScope", "$window", "$location", "configDetailsProvider", "SharedService", "$http", "$timeout", "$q", function($scope, $rootScope, $window, $location, configDetailsProvider, SharedService, $http, $timeout, $q) {

  try {
   // redirect to paymentApp
//   configDetailsProvider.apiConnect.group
   $rootScope.mesgg = [];
   $rootScope.message = "";
   $scope.Loaded = false;

    var checkConfigData = function() {
        $timeout(function (){
        if(configDetailsProvider != undefined && configDetailsProvider.apiConnect.length>0){
        console.log("----configDetaProvider-str--:" + JSON.stringify(configDetailsProvider));
        //   theme color replaced by config color
            document.documentElement.style.setProperty('--main-bg-color', configDetailsProvider.apiConnect[0].color);
            $rootScope.IsShopShow = configDetailsProvider.apiConnect[0].IsShopShow;

              if (configDetailsProvider.apiConnect[0].flag == false) {
               $(".ShopDisabled").css('pointer-events', 'none');
               $(".ShopDisabled").css('opacity', '0.6');
              }
                 setKeysfunc(configDetailsProvider.apiConnect[0])
              // Set Key and Get Data
                 function setKeysfunc(data1) {
                 console.log("----data1------str-----:" + JSON.stringify(data1));
                  var data = {
                   "token": data1.token,
                   "group": data1.group
                  };

                  var url = data1.url;
                  var apiKey = data1.key;
                  var authentication = SHA256(apiKey + JSON.stringify(data));
                  var config = {
                   headers: {
                    "Content-Type": "application/json",
                    "x-authentication": authentication,
                   }
                  };

                  $http.post(url, data, config)
                   .success(function(data, status, headers, config) {
                    $scope.data = JSON.parse(data.data);
                    $scope.isDisableSignup = false;
                    console.log('----StoreList data--:' + $scope.data)
                    console.log('----StoreList data--:' + JSON.stringify($scope.data));
                    sessionStorage["store_list"] = JSON.stringify(data);
                    localStorage["store_listFirst"] = JSON.stringify(data);
              //      console.log("json data  >>=  " + data.data);
                     $scope.Loaded = true;

                   })
                   .error(function(data, status, headers, config) {
                    $rootScope.message = "Data: " + data +
                     "\n status: " + status.toString() +
                     "\n headers: " + headers.toString() +
                     "\n config: " + config.toString();
                    console.log("Data: " + data + "\n status: " + status.toString() +
                     "\n headers: " + headers.toString() +
                     "\n config: " + config.toString());
                   });
                 }

        } else {
            checkConfigData();
        }

        }, 500)
    }
    checkConfigData();

   $(document).ready(function() {

    if (SharedService.getData()) {
     sessionStorage.setItem("siteID", SharedService.getData("storeNumber"));
     var strId = sessionStorage.getItem("siteID");

     //get StoreName with the help of store-number
     var storNameInfo = JSON.parse(localStorage["store_listFirst"]);
     var storNameInfo1 = JSON.parse(storNameInfo.data);
     var indexNumb = storNameInfo1.findIndex(x => x.number == strId);
     var storeName = storNameInfo1[indexNumb].name;
     sessionStorage.setItem("selectedOption", storeName);
    } else {
     $("input:radio").attr("checked", false);
     $('#selection-popup').modal({
      backdrop: 'static',
      keyboard: false
     })
    }
   });

   function checkPermissions() {
    if (sessionStorage.getItem("phoneNumber") || sessionStorage.getItem("EmailId")) {
     $('.messages').removeClass('disable');
    } else {
     $('.messages').addClass('disable');
    }
   }

   checkPermissions();

   var SelValue = "";
   //Sidebar open
   $scope.start = function() {
    $(".sidebar").toggleClass("open");
   }

    //Click to Chat
   $scope.clickToChat = function() {
    if (sessionStorage.getItem("siteID") == null || sessionStorage.getItem("siteID") == "") {
     $location.path("/errorMsg");
    } else {
     var siteID = sessionStorage.getItem("siteID");

     $location.path('/chat/' + siteID);
    }
   }

    //About
   $scope.about = function() {
    $location.path("/about");
   }

    //Redirect Payment
   $scope.redirect = function(mode) {
    sessionStorage.setItem("key", mode);
    $window.location.href = "paymentApp/index.html";
   };

   // set the sessions according to conditions
   if ((localStorage.getItem("siteID") != null) && (localStorage.getItem("userName") != null) && (localStorage.getItem("passWord") != null)) {
    sessionStorage.setItem("siteID", localStorage.getItem("siteID"));
    sessionStorage.setItem("userName", localStorage.getItem("userName"));
    sessionStorage.setItem("passWord", localStorage.getItem("passWord"));
    sessionStorage.loggedUser = "valid";
   } else {

   }

   SharedService.removeSession();

   //check radio button
   $scope.selectRadio = function(value) {
    SelValue = value;
   };

    // set and get StoreNumber
   $scope.storeFunc = function() {
    if (SelValue) {
     SharedService.setData(SelValue);
     sessionStorage.setItem("siteID", SharedService.getData("storeNumber"));
    }
   }

   document.addEventListener('deviceready', function() {
    var permissions = cordova.plugins.permissions;
    permissions.requestPermission(permissions.READ_PHONE_STATE, success, error);

    function error() {
     console.warn('READ_PHONE_STATE permission is not turned on');
    }
    function success(status) {
     if (!status.hasPermission) error();
    }

    permissions.requestPermission(permissions.GET_ACCOUNTS, success, error);

    function error() {
     console.warn('GET_ACCOUNTS permission is not turned on');
    }

//    function success(status) {
//     if (!status.hasPermission) error();
//     getDetails().then(function(email) {});
//    }

       function success(status) {
          if (status.hasPermission ){
              console.log("--------status-----" + status.hasPermission);
              getDetails();
          }
          else if(!status.hasPermission){
            error();
            console.log("--------error-----" + status.hasPermission);
          }
       }

    function getDetails() {
     var defer = $q.defer();
     window.plugins.DeviceAccounts.getEmails(onSuccess, onFail);

    function onSuccess(emails){
    console.log('account registered on this accounts:', emails);
    $scope.deviceAccount = emails;
    SharedService.setEmailId(emails);
    }
   function onFail(error) {
    console.log('Fail to retrieve accounts, details on exception:', error);
   }

     window.plugins.phonenumber.get(success, failed);
     function success(phonenumber) {
      SharedService.setPhoneNumber(phonenumber);
      checkPermissions();
     }

     function failed(error) {
      console.log('Fail to retrieve phonenumber, details on exception:', error);
     }
     return defer.promise;
    }
   }, false);

  } catch (e) {
   console.log(e);
  };
 }]);