// JavaScript Document

$(document).ready(function () {
    resizeContent();
    storeContent();
});
$(window).on('resize', function () {
    resizeContent();
    storeContent();
});

$(window).on('load', function () {
    //resizeContent();
    //storeContent();
});

function resizeContent() {
    if ($(window).width() >= 768) {
        // if larger or equal
        $('#filtercategory').hide();
        $('.filtercollapse').show();

    } else {
        // if smaller
        $('#filtercategory').show();
        $('.filtercollapse').hide();
    }


    //if ($(window).width() >= 991) {
    //    // if larger or equal
    //    $('#storeinfomation').hide();
    //    $('#storeinfo').show();

    //} else {
    //    // if smaller
    //    $('#storeinfomation').show();
    //    $('#storeinfo').hide();
    //}
}
function storeContent() {
    //if ($(window).width() >= 768) {
    //    // if larger or equal
    //    $('#filtercategory').hide();
    //    $('.filtercollapse').show();

    //} else {
    //    // if smaller
    //    $('#filtercategory').show();
    //    $('.filtercollapse').hide();
    //}


    if ($(window).width() >= 991) {
        // if larger or equal
        $('#storeinfomation').hide();
        $('#storeinfo').show();

    } else {
        // if smaller
        $('#storeinfomation').show();
        $('#storeinfo').hide();
    }
}

$('.linkcollapse').on('click', function () {
    $('.filtercollapse').slideToggle('slow');
});



$("#storeinfomation").on('click', function () {
    $("#storeinfo").slideToggle("slow");
});