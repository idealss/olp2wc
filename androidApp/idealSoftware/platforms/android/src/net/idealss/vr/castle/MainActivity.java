
/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package net.idealss.vr.castle;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.red_folder.phonegap.plugin.backgroundservice.DatabaseHandler;

import org.apache.cordova.*;

public class MainActivity extends CordovaActivity
{

    private DatabaseHandler db;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // DatabaseHandler DB=new DatabaseHandler(this);
        //  db=new DatabaseHandler(this);

        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            // moveTaskToBack(true);
        }
        if (extras != null && extras.containsKey("fromNotification") && !extras.getString("fromNotification", "").equals("")) {
            if(extras.getString("fromNotification").equals("message")){
                Log.d("myMSg...", Integer.toString(extras.getInt("myMSg")));
                launchUrl = "file:///android_asset/www/index.html?message="+extras.getString("fromNotification").equals("message")+","+Integer.toString(extras.getInt("myMSg"))+"";
            }
            // moveTaskToBack(true);
        }
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // enable Cordova apps to be started in the background
        Bundle extras = intent.getExtras();

        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }
        if (extras != null && extras.containsKey("fromNotification") && !extras.getString("fromNotification", "").equals("")) {
            if(extras.getString("fromNotification").equals("message")){
                Log.d("myMSg...", Integer.toString(extras.getInt("myMSg")));
                //launchUrl = "file:///android_asset/www/index.html?message="+extras.getString("fromNotification").equals("message")+"";
                launchUrl = "file:///android_asset/www/index.html?message="+extras.getString("fromNotification").equals("message")+",storeID="+Integer.toString(extras.getInt("myMSg"))+"";
            }
            // moveTaskToBack(true);
        }
        //   launchUrl = "file:///android_asset/www/index.html#/chat";
        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }
}
