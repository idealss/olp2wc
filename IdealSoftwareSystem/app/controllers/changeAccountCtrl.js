﻿angular.module('app')
 .controller('changeAcctCtrl',['$scope', '$rootScope', 'idealsApi', function ($scope, $rootScope, idealsApi) {
     
     $scope.oldPass = "";
     $scope.emailAdd = "";
     $scope.newUserName = "";
     $scope.newpass = "";
     $scope.retyPass = "";

     
     try {
         //$rootScope.message = "";
         $scope.userFLName = sessionStorage.userFLName;
       
         $scope.submit = function () {

             //if ($scope.changeAcForm.$valid) {
                 // check to make sure the form is completely valid     
                 $rootScope.ShowLoader = true;
                 $rootScope.message = "";
                 $scope.Name = "";
                 var changeAc = {
                     OldPass: $scope.oldPass,
                     EmailAdd: $scope.emailAdd,
                     NewUserName: $scope.newUserName,
                     Newpass: $scope.newpass,
                     RetyPass: $scope.retyPass
                 };
                 //verify fields and call changeAccountReq method.       
                 idealsApi.changeAccountReq(changeAc.OldPass, changeAc.EmailAdd, changeAc.NewUserName, changeAc.Newpass);                                   
            
         };


         // logout click
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };

         $scope.PaymentHistory = function () {
             idealsApi.payment_historyReq();
         }
     }
     catch(e)
     {
         console.log(e);
     }
}]);