﻿angular.module('app')
 .controller('retrvLoginCtrl', ['$scope', '$rootScope', '$location', 'idealsApi', function ($scope, $rootScope, $location, idealsApi) {
   
     $scope.accountNo = "";
     $scope.emailAdd = "";
     $scope.capchaVal = "";

     try {
         //$rootScope.message = "";


         //$rootScope.message = "";
         Captcha();
         // function to submit the form after all validation has occurred    
         $scope.submit = function () {
      
             // check to make sure the form is completely valid
             if ($scope.retrvForm.$valid) {
              
                 $rootScope.message = "";
                 var isValidCapcha = ValidCaptcha();

                 if (isValidCapcha) {
                     $scope.Name = "";
                     var retrv = {
                         AccountNo: $scope.accountNo,
                         Email: $scope.emailAdd
                     };
                     if (retrv.AccountNo != "" && retrv.Email != "") {
                         $rootScope.ShowLoader = true;
                         idealsApi.retrieveLoginReq(retrv.AccountNo, retrv.Email);
                         var data = "";
                     }
                 }
                 else {
                     $scope.CodeNumberTextbox = "";
                     $scope.accountNo = "";             
                     Captcha();
                     $location.path('/RetrieveLoginInfo');
                 }
             }

         };

         // captcha Code
         function Captcha() {
             var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
             var i;
             for (i = 0; i < 6; i++) {
                 var a = alpha[Math.floor(Math.random() * alpha.length)];
                 var b = alpha[Math.floor(Math.random() * alpha.length)];
                 var c = alpha[Math.floor(Math.random() * alpha.length)];
                 var d = alpha[Math.floor(Math.random() * alpha.length)];
                 var e = alpha[Math.floor(Math.random() * alpha.length)];
                 var f = alpha[Math.floor(Math.random() * alpha.length)];
                 var g = alpha[Math.floor(Math.random() * alpha.length)];
             }
             var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
             $scope.capchaVal = code;
         };
         function ValidCaptcha() {
  
             var string1 = removeSpaces($scope.capchaVal);
             var string2 = removeSpaces($scope.CodeNumberTextbox);    
             if (string1 == string2) {          
                 return true;           
             }
             else {
                 return false;
             }
         };
         function removeSpaces(string) {
             return string.split(' ').join('');
         };



         $scope.Refresh = function () {
             //Captcha Code         
             var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
             var i;
             for (i = 0; i < 6; i++) {
                 var a = alpha[Math.floor(Math.random() * alpha.length)];
                 var b = alpha[Math.floor(Math.random() * alpha.length)];
                 var c = alpha[Math.floor(Math.random() * alpha.length)];
                 var d = alpha[Math.floor(Math.random() * alpha.length)];
                 var e = alpha[Math.floor(Math.random() * alpha.length)];
                 var f = alpha[Math.floor(Math.random() * alpha.length)];
                 var g = alpha[Math.floor(Math.random() * alpha.length)];
             }
             var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
             $scope.capchaVal = code;
         };
          
         $scope.speak = function () {  
                 var text = $scope.capchaVal;
                 responsiveVoice.speak(text, "US English Female", { rate: 0.5 });          
         }
     }
     catch(e)
     {
         console.log(e);
     }


}]);