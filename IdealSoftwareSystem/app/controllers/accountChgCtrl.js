﻿angular.module('app')
 .controller('accountSubmit', ['$scope', '$rootScope', 'idealsApi', function ($scope, $rootScope, idealsApi) {
 
    // logout click
     try {
         $rootScope.message = "";
         $scope.userFLName = sessionStorage.userFLName;
         $scope.logoutClick = function () {
             idealsApi.logoutReq();
         };

         $scope.PaymentHistory = function () {
             idealsApi.payment_historyReq();
         }
     }
     catch(e)
     {
         console.log(e);
     }
}]);